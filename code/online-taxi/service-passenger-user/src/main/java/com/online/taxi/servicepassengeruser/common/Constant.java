package com.online.taxi.servicepassengeruser.common;

/**
 * 业务对象数据常量定义
 */
class Constant {

    /**
     * 乘客缓存 KEY
     */
    static final String CACHE_KEY_PASSENGER = "passenger";

}
